package com.t365.qmm.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName t_cart
 */
@Data
public class Cart implements Serializable {
    /**
     * 购物车id
     */
    private Integer id;

    /**
     * 用户id
     */
    private Integer userid;

    /**
     * 
     */
    private Integer goodsId;

    /**
     * 
     */
    private String goodsName;

    /**
     * 
     */
    private Integer quantity;

    /**
     * 
     */
    private Double price;

    /**
     * 
     */
    private Integer createdby;

    /**
     * 
     */
    private String createddate;

    private static final long serialVersionUID = 1L;
}