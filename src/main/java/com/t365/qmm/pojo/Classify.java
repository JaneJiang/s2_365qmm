package com.t365.qmm.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * 
 * @TableName t_classify
 */
@Data
public class Classify implements Serializable {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 名称
     */
    private String classifyName;

    /**
     * 父级分类ID
     */
    private Integer parentId;

    /**
     * 分类级别(1:一级 2：二级 3：三级)
     */
    private Boolean type;

    /**
     * 图标
     */
    private String icon;

    List<Classify> menuList =new ArrayList<>();//子分类

    private static final long serialVersionUID = 1L;
}