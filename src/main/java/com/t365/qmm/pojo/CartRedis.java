package com.t365.qmm.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * 购物车对象
 */
@Data
public class CartRedis implements Serializable {
    /**
     * 用户id
     */
    private Integer userid;

    private Map<String,GoodsVo> goodsMap =new HashMap<>();  //商品列表

    private static final long serialVersionUID = 1L;
}