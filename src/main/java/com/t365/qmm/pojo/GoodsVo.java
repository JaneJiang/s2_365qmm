package com.t365.qmm.pojo;

import lombok.Data;

/**
 * @author Jane
 * @date 2024-03-05 16:25
 */

@Data
public class GoodsVo extends Goods{

    private Integer count;//购物车商品数量

    private boolean flag=false;
}
