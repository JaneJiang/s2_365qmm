package com.t365.qmm.service;

import com.t365.qmm.pojo.Goods;

public interface GoodsService {
    Goods selectByPrimaryKey(Long id);

    int deleteByPrimaryKey(Long id);

    int insert(Goods record);

    int insertSelective(Goods record);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);
}
