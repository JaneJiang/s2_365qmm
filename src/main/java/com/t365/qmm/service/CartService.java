package com.t365.qmm.service;

import com.t365.qmm.pojo.Cart;

import java.util.List;
import java.util.Map;

/**
 * @author Jane
 * @date 2024-02-29 17:11
 */

public interface CartService {
    int deleteByPrimaryKey(Long id);

    int insert(Cart record);

    int updateByPrimaryKey(Cart record);

    List<Cart> getCartsByCondition(Map<String,Object> maps);
}
