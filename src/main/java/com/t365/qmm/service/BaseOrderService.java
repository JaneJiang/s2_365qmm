package com.t365.qmm.service;

import com.t365.qmm.pojo.BaseOrder;
import com.t365.qmm.pojo.GoodsVo;
import com.t365.qmm.pojo.OrderInfo;

import java.util.List;
import java.util.Map;

/**
 * @Entity com.t365.qmm.pojo.BaseOrder
 */
public interface BaseOrderService {

    int deleteByPrimaryKey(Long id);

    int insert(BaseOrder record);

    int insertSelective(BaseOrder record);

    BaseOrder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseOrder record);

    int updateByPrimaryKey(BaseOrder record);
}
