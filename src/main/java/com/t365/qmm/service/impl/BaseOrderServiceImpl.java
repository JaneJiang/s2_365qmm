package com.t365.qmm.service.impl;

import com.t365.qmm.mapper.BaseOrderMapper;
import com.t365.qmm.mapper.CartMapper;
import com.t365.qmm.pojo.BaseOrder;
import com.t365.qmm.service.BaseOrderService;
import com.t365.qmm.utils.MyabtisUtil;
import org.apache.ibatis.session.SqlSession;

/**
 * @author Jane
 * @date 2024-03-13 16:04
 */

public class BaseOrderServiceImpl implements BaseOrderService {
    @Override
    public int deleteByPrimaryKey(Long id) {
        return 0;
    }

    @Override
    public int insert(BaseOrder record) {

        //新增
        SqlSession session= null;
        try {
            session = MyabtisUtil.createSqlSession_tran(false);
            session.getMapper(BaseOrderMapper.class).insert(record);
            session.commit();
        } catch (Exception e) {
            session.rollback();
        } finally {
            MyabtisUtil.closeSqlSession(session);
        }
        return -1;

    }

    @Override
    public int insertSelective(BaseOrder record) {
        return 0;
    }

    @Override
    public BaseOrder selectByPrimaryKey(Long id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(BaseOrder record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(BaseOrder record) {
        return 0;
    }
}
