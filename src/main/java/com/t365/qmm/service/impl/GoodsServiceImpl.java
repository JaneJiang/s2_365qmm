package com.t365.qmm.service.impl;

import com.t365.qmm.mapper.BaseOrderMapper;
import com.t365.qmm.mapper.GoodsMapper;
import com.t365.qmm.pojo.Goods;
import com.t365.qmm.service.GoodsService;
import com.t365.qmm.utils.MyabtisUtil;
import org.apache.ibatis.session.SqlSession;

import java.sql.Connection;

/**
 * @author Jane
 * @date 2024-02-26 16:59
 */

public class GoodsServiceImpl implements GoodsService {


    @Override
    public Goods selectByPrimaryKey(Long id) {
        SqlSession session= MyabtisUtil.createSqlSession();
        Goods goods = session.getMapper(GoodsMapper.class).selectByPrimaryKey(id);
        MyabtisUtil.closeSqlSession(session);
        return goods;
    }

    @Override
    public int deleteByPrimaryKey(Long id) {
        return 0;
    }

    @Override
    public int insert(Goods record) {
        return 0;
    }

    @Override
    public int insertSelective(Goods record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeySelective(Goods record) {
        //修改
        SqlSession session= null;
        try {
            session = MyabtisUtil.createSqlSession_tran(false);
            session.getMapper(GoodsMapper.class).updateByPrimaryKeySelective(record);
            session.commit();
        } catch (Exception e) {
            session.rollback();
        } finally {
            MyabtisUtil.closeSqlSession(session);
        }
        return -1;
    }

    @Override
    public int updateByPrimaryKey(Goods record) {
       return 0;
    }
}
