package com.t365.qmm.service.impl;

import com.t365.qmm.mapper.BaseOrderMapper;
import com.t365.qmm.mapper.OrderInfoMapper;
import com.t365.qmm.pojo.GoodsVo;
import com.t365.qmm.pojo.OrderInfo;
import com.t365.qmm.service.OrderInfoService;
import com.t365.qmm.utils.MyabtisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.Map;

/**
 * @author Jane
 * @date 2024-03-13 16:17
 */

public class OrderInfoServiceImpl implements OrderInfoService {
    @Override
    public int deleteByPrimaryKey(Long id) {
        return 0;
    }

    @Override
    public int insert(OrderInfo record) {
        //新增
        SqlSession session= null;
        try {
            session = MyabtisUtil.createSqlSession_tran(false);
            session.getMapper(OrderInfoMapper.class).insert(record);
            session.commit();
        } catch (Exception e) {
            session.rollback();
        } finally {
            MyabtisUtil.closeSqlSession(session);
        }
        return -1;
    }

    @Override
    public int insertSelective(OrderInfo record) {
        return 0;
    }

    @Override
    public OrderInfo selectByPrimaryKey(Long id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(OrderInfo record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(OrderInfo record) {
        return 0;
    }

    @Override
    public void insert(Integer id, Map<String, GoodsVo> goodsMap) {

    }
}
