package com.t365.qmm.service.impl;

import com.t365.qmm.mapper.CartMapper;
import com.t365.qmm.pojo.Cart;
import com.t365.qmm.service.CartService;
import com.t365.qmm.utils.MyabtisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jane
 * @date 2024-02-29 17:11
 */

public class CartServiceImpl implements CartService {
    @Override
    public int deleteByPrimaryKey(Long id) {
        return 0;
    }

    @Override
    public int insert(Cart cart) {
        //新增
        SqlSession session= null;
        try {
            session = MyabtisUtil.createSqlSession();
            return session.getMapper(CartMapper.class).insert(cart);
        } catch (Exception e) {
            session.rollback();
        } finally {
            MyabtisUtil.closeSqlSession(session);
        }
        return -1;
    }

    @Override
    public int updateByPrimaryKey(Cart cart) {
        //新增
        SqlSession session= null;
        try {
            session = MyabtisUtil.createSqlSession();
            return session.getMapper(CartMapper.class).updateByPrimaryKey(cart);
        } catch (Exception e) {
            session.rollback();
        } finally {
            MyabtisUtil.closeSqlSession(session);
        }
        return -1;
    }

    @Override
    public List<Cart> getCartsByCondition(Map<String, Object> maps) {
        SqlSession session= MyabtisUtil.createSqlSession();
        List<Cart> cartList = session.getMapper(CartMapper.class).getCartsByCondition(maps);
        MyabtisUtil.closeSqlSession(session);
        return cartList;
    }
}
