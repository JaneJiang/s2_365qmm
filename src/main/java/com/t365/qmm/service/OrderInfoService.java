package com.t365.qmm.service;

import com.t365.qmm.pojo.GoodsVo;
import com.t365.qmm.pojo.OrderInfo;

import java.util.Map;

/**
 * @Entity com.t365.qmm.pojo.OrderInfo
 */
public interface OrderInfoService {

    int deleteByPrimaryKey(Long id);

    int insert(OrderInfo record);

    int insertSelective(OrderInfo record);

    OrderInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(OrderInfo record);

    int updateByPrimaryKey(OrderInfo record);

    void insert(Integer id, Map<String, GoodsVo> goodsMap);
}
