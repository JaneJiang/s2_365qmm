package com.t365.qmm.servlet;

import com.t365.qmm.service.GoodsService;
import com.t365.qmm.service.impl.GoodsServiceImpl;
import com.t365.qmm.utils.Constants;
import com.t365.qmm.utils.RedisUtil;
import redis.clients.jedis.Jedis;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CartListServlet", value = "/CartListServlet")
public class CartListServlet extends HttpServlet {
    Jedis jedis = RedisUtil.getJedis();
    GoodsService goodsService = new GoodsServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("--------------------------------CartListServlet");
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        //从redis中请求到该用户的购物车信息
        Integer userId =12 ;//省略session逻辑
        String key = Constants.CART_PREFIX.concat(userId.toString());
        String cart_json="";

        Boolean exists = jedis.exists(key);
        if (exists){
            cart_json = jedis.get(key);
        }
        out.print(cart_json);

        out.flush();
        out.close();

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
