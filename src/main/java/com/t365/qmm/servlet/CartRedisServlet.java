package com.t365.qmm.servlet;

import com.t365.qmm.service.GoodsService;
import com.t365.qmm.service.impl.GoodsServiceImpl;
import com.t365.qmm.utils.RedisUtil;
import redis.clients.jedis.Jedis;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Jane
 * @date 2024-03-05 17:24
 */
@WebServlet("/CartRedisServlet")
public class CartRedisServlet extends HttpServlet {
    Jedis jedis = RedisUtil.getJedis();
    GoodsService goodsService = new GoodsServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("--------------------------------CartRedisServlet");
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=UTF-8");

        PrintWriter out = resp.getWriter();

        String opr = req.getParameter("opr");

        Integer userId= 2;
        Integer goodsId=Integer.parseInt(req.getParameter("goodsId")==null?"":req.getParameter("goodsId")) ;
        ////用户填写的购买数量
        Integer count=Integer.parseInt(req.getParameter("count")==null?"":req.getParameter("count")) ;
        String key = "t365cart:"+userId;
        String cart_json="";

        HttpSession session = req.getSession();

        out.print("ok" +goodsId +","+count);

        out.flush();
        out.close();


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
