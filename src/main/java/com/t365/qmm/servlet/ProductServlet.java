package com.t365.qmm.servlet;

import com.alibaba.fastjson.JSON;
import com.t365.qmm.pojo.Goods;
import com.t365.qmm.service.GoodsService;
import com.t365.qmm.service.impl.GoodsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author Jane
 * @date 2024-02-26 16:53
 */
@WebServlet("/ProductServlet")
public class ProductServlet extends HttpServlet {
    GoodsService goodsService ;

    public ProductServlet(){
        goodsService =new GoodsServiceImpl();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        String pid =req.getParameter("pid");
        Long _pid= Long.valueOf(pid);

        //按照pid去后台执行查询功能
        Goods goods =goodsService.selectByPrimaryKey(_pid);
        //将数据保存到request  / session
        //String product = JSON.toJSONString(goods);
        //out.print(product);
//        System.out.println(goods);

         req.setAttribute("product",goods);
        //转发 至Product.jsp
        req.getRequestDispatcher("Product.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
