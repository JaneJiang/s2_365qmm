package com.t365.qmm.test;

import com.alibaba.fastjson.JSON;
import com.t365.qmm.pojo.CartRedis;
import com.t365.qmm.pojo.Goods;
import com.t365.qmm.pojo.GoodsVo;
import com.t365.qmm.service.GoodsService;
import com.t365.qmm.service.impl.GoodsServiceImpl;
import com.t365.qmm.utils.RedisUtil;
import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * @author Jane
 * @date 2024-03-05 16:14
 */

public class TestCart {

    Jedis jedis = RedisUtil.getJedis();
    GoodsService goodsService = new GoodsServiceImpl();
    Integer userId= 12;
    Integer _goodsId=737;
    String goodsId =_goodsId.toString();
    Integer count=2; //用户填写的购买数量
    String key = "t365cart:"+userId;
    String cart_json="";
    @Test
    public void addCart(){

        //根据用户id查看该用户是否有购物车
        boolean flag = false;
        if (flag){ //有
            //从redis该用户的购物车信息

            //判断商品是否已经在购物车中
            if (flag){ //是 数量累加

            }else{ //否 在当前购物车添加一笔数据

            }

        }else{ //无
            //添加购物车
            CartRedis cart =new CartRedis();
            cart.setUserid(userId);

            //商品详情
            Goods goods = goodsService.selectByPrimaryKey(Long.valueOf(goodsId));

            GoodsVo good1 = new GoodsVo();
            good1.setId(goods.getId());
            good1.setGoodsName(goods.getGoodsName());
            good1.setFileName(goods.getFileName());
            good1.setPrice(goods.getPrice());
            good1.setCount(count); //数量

            cart.getGoodsMap().put(goodsId,good1);

            String cart_json = JSON.toJSONString(cart);

            //存入redis


            jedis.set(key,cart_json);

            System.out.println("成功加入购物车");
        }


    }

    @Test
    public void updateCart() {
        boolean flag = jedis.exists(key);
        if (flag){
            String cart_json = jedis.get(key);
            CartRedis cart = JSON.parseObject(cart_json,CartRedis.class);//从redis中获取购物车
            GoodsVo goodsVo = cart.getGoodsMap().get(goodsId);
            if (null!=goodsVo){
                //是 数量累加
                int nums =goodsVo.getCount()+count;//原始数量
                goodsVo.setCount(nums) ;
                cart.getGoodsMap().put(goodsId,goodsVo);

            }else{ //购物车添加一款商品
                changeCart(cart);
            }
            cart_json=JSON.toJSONString(cart);
            jedis.set(key,cart_json);

        }else{
            System.out.println("没有购物车");
            CartRedis cart =new CartRedis();
            changeCart(cart);
            cart_json=JSON.toJSONString(cart);

            jedis.set(key,cart_json);
        } 
    }

    private void changeCart(CartRedis cart) {
        //商品详情
        Goods goods = goodsService.selectByPrimaryKey(Long.valueOf(goodsId));

        GoodsVo good1 = new GoodsVo();
        good1.setId(goods.getId());
        good1.setGoodsName(goods.getGoodsName());
        good1.setFileName(goods.getFileName());
        good1.setPrice(goods.getPrice());
        good1.setCount(count); //数量

        cart.getGoodsMap().put(goodsId,good1);
    }

    @Test
    public void delCart(){//删除购物车
        //存在
        boolean flag = jedis.exists(key);
        if (flag){

            String cart_json = jedis.get(key);
            CartRedis cart = JSON.parseObject(cart_json,CartRedis.class);//从redis中获取购物车
            GoodsVo goodsVo = cart.getGoodsMap().get(goodsId);
            if (null!=goodsVo){

                cart.getGoodsMap().remove(goodsId);//.put(goodsId,goodsVo);

            }else{ //购物车添加一款商品
                changeCart(cart);
            }
            cart_json=JSON.toJSONString(cart);
            jedis.set(key,cart_json);

        }
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        Scanner input = new Scanner(System.in);
        System.out.println("请输入密码:");
        String password = input.next();

        String hexStr ="";

        MessageDigest md = MessageDigest.getInstance("MD5");

        byte[] digest = md.digest(password.getBytes(StandardCharsets.UTF_8));//乱码

        hexStr = Hex.encodeHexString(digest);

        if (hexStr.equals("e10adc3949ba59abbe56e057f20f883e")){
            System.out.println("登录成功");
        }else{
            System.out.println("登录失败");
        }

        //System.out.println(hexStr);

    }
}
