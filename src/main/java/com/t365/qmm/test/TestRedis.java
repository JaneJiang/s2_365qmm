package com.t365.qmm.test;

import org.junit.jupiter.api.Test;
import redis.clients.jedis.Jedis;

/**
 * @author Jane
 * @date 2024-03-05 15:45
 */

public class TestRedis {

    @Test
    public void helloRedis(){
        //连接数据库
        Jedis jedis = new Jedis("localhost");
        //设置k-v
        jedis.auth("123456");
        jedis.set("t365","玖谊科技");
        //取值
        System.out.println("添加成功");

    }

    @Test
    public void getValue(){
        //连接数据库
        Jedis jedis = new Jedis("localhost");
        //设置k-v
        jedis.auth("123456");
        //取值
        String t365 = jedis.get("t365");
        System.out.println(t365);

    }

}
