package com.t365.qmm.utils;

public interface Constants {

    String ERR_CODE_001="001";
    String ERR_MSG_001="数据库连接失败";

    String ERR_CODE_1001="1001";
    String ERR_MSG_1001="用户输入格式不正确";
    String ERR_MSG_1002="请先选择商品再结算";

    String CART_PREFIX ="t365cart:";
}
