package com.t365.qmm.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Jane
 * @date 2024-01-17 16:28
 *
 * 非集成环境下的最佳实践
 * SqlSessionFactoryBuilder的最佳使用范围是什么？
 * 用过即丢
 * 推荐作用域范围：局部变量
 *
 * SqlSessionFactory的最佳使用范围是什么？
 * 生命周期与应用的生命周期相同
 * 最佳作用域范围：应用的全局作用域
 *
 * SqlSession的最佳使用范围是什么？
 * 线程级
 * 一个request请求期间
 */

public class MyabtisUtil {
    private  static SqlSessionFactory factory;

    static {
        try {
            InputStream is = Resources.getResourceAsStream("mybatis_config.xml");
            factory=new SqlSessionFactoryBuilder().build(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 打开链接
     * @return
     */
    public static SqlSession createSqlSession(){
        //魔法值  编码的过程中尽量避免出现魔法值
        return factory.openSession(true);//开启事务自动提交
    }
    public static SqlSession createSqlSession_tran(Boolean flag){
        return factory.openSession(flag);
    }
    //关闭链接
    public static void closeSqlSession(SqlSession session){
        session.close();
    }
}
