package com.t365.qmm.mapper;

import com.t365.qmm.pojo.BaseOrder;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

/**
 * @Entity com.t365.qmm.pojo.BaseOrder
 */
public interface BaseOrderMapper {

    int deleteByPrimaryKey(Long id);

    int insert(BaseOrder record);

    int insertSelective(BaseOrder record);

    BaseOrder selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BaseOrder record);

    int updateByPrimaryKey(BaseOrder record);

}
