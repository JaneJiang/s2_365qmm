package com.t365.qmm.mapper;

import com.t365.qmm.pojo.User;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Entity com.t365.qmm.pojo1.User
 */
public interface UserMapper {

    int deleteByPrimaryKey(Long id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    /**
     * Condition 条件
     * @param maps
     * @return
     */
    List<User> getUserListByCondition(Map<String,String> maps);

}
