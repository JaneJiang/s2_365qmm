package com.t365.qmm.mapper;

import com.t365.qmm.pojo.Classify;

import java.util.List;
import java.util.Map;

/**
 * @Entity com.t365.qmm.pojo.Classify
 */
public interface ClassifyMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Classify record);

    int insertSelective(Classify record);

    Classify selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Classify record);

    int updateByPrimaryKey(Classify record);

    List<Classify> getAllByCondition(Map<String,String> maps);

}
