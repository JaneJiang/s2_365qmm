package com.t365.qmm.mapper;

import com.t365.qmm.pojo.Cart;

import java.util.List;
import java.util.Map;

/**
 * @Entity com.t365.qmm.pojo.Cart
 */
public interface CartMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Cart record);

    int insertSelective(Cart record);

    Cart selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKey(Cart record);

    List<Cart> getCartsByCondition(Map<String,Object> maps);

}
