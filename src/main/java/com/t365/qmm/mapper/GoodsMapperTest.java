package com.t365.qmm.mapper;

import com.t365.qmm.pojo.Goods;
import com.t365.qmm.utils.MyabtisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GoodsMapperTest {

    @Test
    void selectByPrimaryKey() {
        SqlSession session= MyabtisUtil.createSqlSession();
        Goods goods = session.getMapper(GoodsMapper.class).selectByPrimaryKey(733L);
        MyabtisUtil.closeSqlSession(session);
    }
}